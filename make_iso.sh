#!/usr/bin/env bash

sudo rm -f ~/livecd/iso/casper/filesystem.squashfs
sudo mksquashfs ~/livecd/rootfs/ ~/livecd/iso/casper/filesystem.squashfs -comp lz4
# создаем filesystem.manifest
sudo chroot ~/livecd/rootfs dpkg-query -W --showformat='${Package} ${Version}\n' | sudo tee ~/livecd/iso/casper/filesystem.manifest
sudo cp -v ~/livecd/iso/casper/filesystem.manifest ~/livecd/iso/casper/filesystem.manifest-desktop

# обновляем файл filesystem.size
printf $(sudo du -sx --block-size=1 ~/livecd/rootfs | cut -f1) > ~/livecd/iso/casper/filesystem.size

# считаем md5 hash сумму
cd ~/livecd/iso
sudo rm md5sum.txt
find . -type f -print0 | sudo xargs -0 md5sum | grep -v isolinux/boot.cat | sudo tee md5sum.txt
# сборка iso образа
sudo xorriso -as mkisofs \
	  -r -J -V "greenlinux-2023.0.0.1" \
	  -b isolinux/isolinux.bin \
	  -c isolinux/boot.cat \
	  -no-emul-boot \
	  -partition_offset 16 \
	  -boot-load-size 4 \
	  -boot-info-table \
	  -isohybrid-mbr "/usr/lib/ISOLINUX/isohdpfx.bin" \
	  -o ~/greenlinux-2023.0.0.1.iso \
	  ~/livecd/iso