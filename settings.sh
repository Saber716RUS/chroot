#!/usr/bin/env bash
apt remove -y firefox libreoffice rhythmbox celluloid transmission-common transmission-gtk && apt purge -y firefox libreoffice rhythmbox celluloid transmission-common transmission-gtk mintupgrade mint-upgrade-info mintwelcome && apt purge -y hexchat hexchat-common hunspell-de-at-frami hunspell-de-ch-frami hunspell-de-de-frami hunspell-en-au hunspell-en-ca hunspell-en-gb hunspell-en-za hunspell-es hunspell-fr hunspell-fr-classical hunspell-it hunspell-pt-br hunspell-pt-pt hyphen-de hyphen-en-ca hyphen-en-gb hyphen-fr hyphen-it hyphen-pt-br hyphen-pt-pt libreoffice-l10n-de libreoffice-l10n-en-gb libreoffice-l10n-en-za libreoffice-l10n-es libreoffice-l10n-fr libreoffice-l10n-it libreoffice-l10n-pt libreoffice-l10n-pt-br libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw librhythmbox-core10 mythes-de mythes-de-ch mythes-en-au mythes-fr mythes-it mythes-pt-pt fonts-beng-extra fonts-deva-extra fonts-gargi fonts-gubbi fonts-gujr-extra fonts-guru-extra fonts-kacst* fonts-kalapi fonts-khmeros* fonts-lao fonts-lklug-sinhala fonts-lohit* fonts-nakula fonts-nanum* fonts-navilu fonts-noto-cjk fonts-noto-mono fonts-orya-extra fonts-pagul fonts-sahadeva fonts-samyak* fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-smc* fonts-takao* fonts-telu* fonts-tibetan-machine fonts-tlwg* fonts-wqy* fonts-yrsa-rasa
apt autoremove -y
dpkg --add-architecture i386
apt update
apt install -y vlc audacious qbittorrent libnss3 icoutils libvulkan1 libvulkan1:i386 cabextract bc libgl1-mesa-glx libgl1-mesa-glx:i386 gamemode libjpeg62-turbo:i386 libcupsimage2:i386
wget  https://gu-st.ru/content/lending/russian_trusted_root_ca_pem.crt
wget  https://gu-st.ru/content/lending/russian_trusted_sub_ca_pem.crt
mkdir /usr/local/share/ca-certificates/russian_trusted
cp russian_trusted_root_ca_pem.crt russian_trusted_sub_ca_pem.crt /usr/local/share/ca-certificates/russian_trusted
update-ca-certificates -v
