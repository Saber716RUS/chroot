#!/usr/bin/env bash
# Очистка предудущего образа и chroot

cd /tmp || exit
sudo rm -f lmde-6-cinnamon-64bit.iso
sudo rm -rf ~/livecd/
# загрузка, монтирование, распаковка файла squashfs и копирование настроечного скрипта chroot
sudo mkdir /mnt/iso
wget https://mirror.yandex.ru/linuxmint/debian/lmde-6-cinnamon-64bit.iso
sudo mount -o loop lmde-6-cinnamon-64bit.iso /mnt/iso
sudo mkdir -p ~/livecd/iso/
sudo cp -rf /mnt/iso/* ~/livecd/iso/
sudo umount /mnt/iso
sudo rm -rf /mnt/iso
sudo unsquashfs -f -d ~/livecd/rootfs ~/livecd/iso/casper/filesystem.squashfs
cd /tmp && wget https://gitlab.linuxmint.su/Saber716RUS/chroot/-/archive/main/chroot-main.zip && unzip chroot-main.zip && rm -r chroot-main.zip
cd ~/chroot && chmod +x settings.sh && sudo cp -fr settings.sh ~/livecd/rootfs && rm -fr /tmp/chroot-main
sudo cp -fr /etc/resolv.conf ~/livecd/rootfs/etc/resolv.conf
sudo cp -fr /etc/os-release ~/livecd/rootfs/etc/os-release
sudo mount --bind /proc ~/livecd/rootfs/proc
sudo mount --bind /dev ~/livecd/rootfs/dev
sudo mount --bind /sys ~/livecd/rootfs/sys
# запуск chroot c настроечным скриптом
cd ~/livecd/rootfs/ || exit
sudo chroot ~/livecd/rootfs/ su -lc /settings.sh
# размонтирование
sudo umount -lR ~/livecd/rootfs/proc
sudo umount -lR ~/livecd/rootfs/dev
sudo umount -lR ~/livecd/rootfs/sys
cd /tmp && cp ~/chroot/make_iso.sh /tmp/make_iso.sh && chmod +x make_iso.sh && ./make_iso.sh